Rails.application.routes.draw do
  resources :events
  resources :schedules
  root 'top#index'
  
  resource :schedules, only: [:new]
  resource :events, only: [:index, :show]
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
end
