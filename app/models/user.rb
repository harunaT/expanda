class User < ApplicationRecord
  include Ai
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: [:facebook]
         
  acts_as_taggable_on :interests
  
  enum user_type: { guest: 0, host: 1 }
         
  def self.find_for_oauth(auth)
    user = User.where(uid: auth.uid, provider: auth.provider).first
    
    unless user
      user = User.create(
        token:    auth.credentials.token,
        uid:      auth.uid,
        provider: auth.provider,
        email:    auth.info.email,
        image_url: auth.info.image,
        password: Devise.friendly_token[0, 20]
      )
    else
      user.update(token: auth.credentials.token)
    end
    
    graph = Koala::Facebook::API.new(user.token)
    events = graph.get_object("me?fields=events")
    descriptions = []
    events['events']['data'].each do |event|
      descriptions.push(event['description'])
    end
    
    if descriptions.present?
      user.interest_list = Ai.classification(descriptions)
      user.save
    end
    user
  end

  private

  def self.dummy_email(auth)
    "#{auth.uid}-#{auth.provider}@example.com"
  end
end
