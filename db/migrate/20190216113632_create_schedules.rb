class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.references :user, null: false, index: true, foreign_key: true
      t.integer :month, null: false, default: 0
      t.integer :budget, null: false, default: 0
      t.datetime :schedule
      t.timestamps
    end
  end
end
