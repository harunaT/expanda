class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.references :user, null: false, index: true, foreign_key: true
      t.string :title, null: false, default: 0
      t.text :description, null: false, default: 0
      t.integer :budget, null: false, default: 0
      t.datetime :schedule
      t.timestamps
    end
  end
end
