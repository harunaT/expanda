require 'csv'

csv = CSV.read('db/fixtures/categories/categories.csv')
csv.each_with_index do |l, i|
  id = i + 1
  name = l[0]

  Category.seed(:id) do |category|
    category.id = id
    category.name = name
  end
end
